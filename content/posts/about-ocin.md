---
title: "About OCIN Uganda"
date: 2018-01-28T20:33:06+01:00
draft: false
---

![OCIN Uganda](https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-9/23167889_1509461969140822_625576595626800454_n.jpg?oh=7189d3fef1d35a5aa026f1cdac2a7a95&oe=5B262D15)

<b>Orphans and Children in Need Uganda</b>

P.O.BOX 4821<br>
JINJA-UGANDA<br>
EAST AFRICA<br>

+256 772 178 790<br>
+256 757 327 555<br>

<a mailto:ocinuganda@gmail.com>ocinuganda@gmail.com</a><br>

<a href="https://www.facebook.com/OCINUganda" target="_blank">OCIN Uganda on Facebook</a>

<hr><br>

INTRODUCTION AND BACKGROUND

OCIN Uganda is a total child care ministry committed to rescue and meet the basic needs of the suffering children and orphans who have lost their parents due to HIV/AIDS mainly though there might be other factors these include family splits, poverty, civil wars and internal displacement.

The ministry office is located in kadiba zone in Buwenge Town council market street Rhema Faith church road. The vision of this ministry was conceived in 2011 and then started its charity services in 2012. On 7th-May-2013 registered as a community based Organization in Buwenge Town council.

The ministry was founded and is led by Mr. Brandon Nasitu Magadha whom God touched to help the suffering, desperate and abandoned children who were roaming on the streets of Buwenge as Hawkers. It reached a time where he could not help it out until he acted with the help and guidance of his pastor then Orphans and children in need began to receive partial support.

<br>
WHY THE CONCERN OF ORPHANS AND CHILDREN IN NEED

Every person, ever conceived and born deserves love, care, food, shelter, security, knowledge that critical needs will continue to be met, bonding, mentoring and nurturing which will lead to a sense of belonging as well as self-actualization or self-worth to belief that one is of value to themselves and others.  Children will first seek their critical needs and the supply of those critical needs will instill a learned behavior. And then they will seek other needs as well and the source of these needs will again become a behavior. For example, if a starving child learns that the surest way to not be hungry is to steal they will learn that behavior as means of survival. If being for Example, Child soldier or terrorist means that they are fed, clothed and receive attention and praise from an adult figure, Again they will learn that these types of behavior bring them rewards…….Supplying them with their needs. In a sense they will become what they have learned in their struggle to survive and belong.

 Consider this article written about the Taliban, Aliqueidha. In Essence, every orphan child will either perish or receive his or her basic human needs from some source good or bad. It is up those who have the means and the opportunity to be sure that the source is good to provide for them. I f we don’t, it will be the pimps, slave traders ill-willed relatives thieves, gangsters, warlords and terrorists that do;

No human is ever born to be unhappy, unhealthy, uneducated but due to life incidentals that come in unexpectedly causes such tragedies to happen.

However, there are many children who are born into loving homes in which they will have the right that every child deserves. There has been a single orphan or children in need Born, that chose or deserves their status as a child without parental care whether you believe or not  it is God’s will as stated in the scriptures such as James 1:27 or simply understand our inherent responsibility to care for orphans, widows and protect their future. It’s plain to see that the orphan children of this world are something we simply cannot ignore. Its these children who have no one to care that are most vulnerable to being brought up in lives of crime, sold off into slavery, sexual exploitation, child soldiers. They are the most vulnerable, because they are children ,these children will make choices solely out of necessity. Just like anyone, they will do what they need to survive. That’s a decision a child should not have to make at such a crucial age. We care for Orphans and children in need because they are the future.
OUR MAJOR GOAL.

Is to end the pain and sorrows that orphans and children in need go through, to create a smile by promoting the children’s rights, empower their families for self sustainability.

OCIN-UGANDA: means to care and showing love for the suffering as we build a bridge to a brighter future seeking to restore and resurrecting the dead dreams. OCIN-Uganda is a relational ministry that is capturing the call of our creator to defend the cause of the weak and fatherless to maintain the right of poor and oppressed psalms 82:3

<br>
MISSION OF OCIN-UGANDA

Rescue, rehabilitate and restore the orphans and children in need to God’s glory.

<br>
VISION OF OCIN-UGANDA

Raise a God fearing generation of leaders, self reliant citizens and excelling professions.

<br>
OBJECTIVES OF OCIN-UGANDA

a. To create a sense of belonging; We have realized that every child needs love acceptance and recognition, so its in OCIN-Uganda where these feelings can be experienced.

b. To ensure that the children feel welcomed and they experience God’s love in word and  action.

c. To introduce children and orphans to the love of God (because whoever does not love, does not know God, For God is love John 4:8.

d. To show children the way to develop a relationship with God “Jesus said am the way, the truth and the life No one comes to father except through me John 14:6.

e. To train the children and orphans in the way should go and when they grow old they will not depart from it. Proverbs 22:6.

f. To Encourage children to love the lord with all their hearts and will all there soul and with their entire mind Mathew 22:37.

g. To establish in children habits of Godly living as daily life style.

h. To create a Christian community of children and adults who will enjoy life together as a family and are committed to Each other and to Jesus Christ.

i. To help children grow from being self centered to becoming Christ-centered, focusing on others rather than on themselves.

j. To make each child’s experience safe, fun and profitable in the process of becoming everything God has planned.

k. To restore hope and positive expectation as well as impact the spirit of creativity in the live of orphans and children in need.

l. To stop enslavement and glossily abusive of orphans and children in need through engagement in house help activities, street-hawking, child labor as well as practice of early girl marriage and brutality.

m. To bring an economical change within the faster families by initiating income generating projects which makes the adopted child to be seen as a blessing within the family.

n. To love care for orphans and children in need who have dropped out of school, lost their parents to civil wars, HIV/AIDS and those suffering the effects of poverty.

o. To voice out the needs of the orphans and children in need and become a bridge between those that are willing to provide support and children’s guardian family here in Africa.

p. To establish a child sponsorship program that will enable an orphan and child in need access education from a family, church or an individual in developed countries.

q. To train the orphans and children in need with self reliance skills and income generating activities that will provide hands- on JOB experience not over looking crafts making, tailoring, saloon and Hair addressing, shoes sawing brick making.

r. To promote local and international partnership as means of addressing the needs of orphans and children in need by sourcing skills, man power, donations and grants to stop undulation and the suffering.

s. To establish income generating activities or projects that can ensure the ministry’s sustainability without an external help (Vision 2030).

t. To establish shelter and safe nourishing environment for the total orphans and children in need who have no guardian family to stay. For Example the abandoned children, children at risk and those rescued from streets.

u. To up bring the talents of the children and orphans because each child is born with enough talents, gifts and resources to make unique contribution to the community and the world.

v. To develop future leaders in all spheres of life through focusing on three key areas: learning by experience learning through others and training.

w. To create a happy, secure and orderly environment in which orphans and children in need can learn and develop as caring and responsible individuals in society.

<br>
OCIN CORE VALUES: RESPECT, COMPASSION AND DIGNITY

This core value affirms that each person has a special value, unique talents and varied gifts. When we revere life, we act with deep respect and compassion for the dignity and diversity of life. The following behaviors and attitudes show how we attempt to do just that each and every day. To heal the physical bodies, the spiritual areas and emotional needs of hurting generation.

INTEGRITY: This core value suggests we should inspire trust through personal leadership that our words and actions should be consistent with our values in day to day terms, living with integrity may be recognized as how we hold our selves’ personally accountable to do what is right and ethically communicate with honesty and directness.

WISDOM AND INTELLIGENCE:
This core value suggests that everyone within our organization is committed to the principles of performance improvements to foster a service culture more specially; Evaluate options and concerns. Act to reflect and understand of excellence and stewardship that includes cultivating relationships holistic use of resources, and also balance work and home demands for a better quality of life

CREATIVITY-INNOVATION: This core value affirms our commitment to seek new approaches to survive and care, to willingly try new approaches with vitality, Energy and enthusiasm. This value is exemplified when we seek new approaches in preferences see challenges as an opportunity for resourcefulness. Embrace change with hope and courage always show our dedication by refining our ideas.

<br>
WHO DO WE SERVE?

* Total Orphans: Children who have lost both parent that’s to say father and mother to either AIDS or Ebola etc.

* Half Orphans: Children who have lost one of the two parents, that is to say mother or father. There is always a struggle to maintain a equilibrium.

* Poor and Needy Children: Both parents are alive but economically not functioning to meet the needs of the family. In most cases they are denied Education, Medical care and other benefits.

* Abandoned children: Children who are deserted without any regard for their physical health, safety, and provision of necessary care; in some instances the child is left at a stranger’s doorstep.
* Street Children: These are minors who live and survive on the streets because of conflicts with their families, abuse, and harassment and don’t want to return home. They often grow in up in public landfills, under bridges, bushes and unfinished homes under construction.

<br>
CATCHMENT AREAS OF OCIN-UGANDA

The following areas benefit from Ocin-Uganda care.

* Buwenge town council
* Namarele
* Muguluka
* Buweera east
* Buwolero
* Nawanyago

<br>
MINISTRIES AT OCIN-UGANDA

Evangelism: in this ministry we are led to call and reclaim the lost both children young adults that are prodigal and can be saved by trusting Christ aiming at restoring the broken relationship between man and God.
Seminars: in this area we educate orphans and children in need youth on topics basing on biblical truth systematically through carefully prepared materials. Child discipline, finance, sex, Aids and the dangers.

Community visit and outreach: we aim at developing a heart of volunteerism, patriotism as we instill formal and informal Education.

Hiv/Aids Awareness Campaign.  We Educate the orphans and children in need, youth and community members the dangers of sexually transmitted diseases mainly HIV/AIDS, we encourage the youth to abstain from sex until the right time (time of marriage)

We also encourage parents to remain faithful and stick to one partner. This is done through sports events, music dance and drama.

<br>
PROJECTS AT OCIN-UGANDA

Back to school project: this project offers opportunities for school dropouts as we improve for ways to get them back to school initially we provide for exercise books ,pens, pencils and clothes and some small start up money for registration (interviews) as we believe God for his mega provision. Through this program we also educate parents the value of educating their children for those who are reluctant to pay school fees.

Child sponsorship project: This program helps the less fortunate as they are fully catered for in Education. That’s to say a child obtains quality day services. Through this program we create opportunities for individuals, families, companies or organization from developed countries to sponsor or fund a child in need or an orphan on a monthly basis until this child becomes self sufficient.

The money received from the sponsor meets Education bills (school fees), provision of scholastic materials, health care, shelter and security, clean water, food, clothes extra.

House Hold Empowerment and Sustainability Project: This is intended to build the financial capacity of the household where an Orphan or Needy child supported by Ocin Uganda is identified. This is done by offering an income generating activity to that house hold   

<br>
SELFSUSTAINABILITY VISION 2030

At OCIN-Uganda sees the importance of having enough funds, materials and resources to protect the future of the organization even when the interdependent economy of world changes that it cannot suffer from its effects.

The following are the projects we intend to establish:
* Brick making and production.
* Piggery rearing.
* Poultry keeping.
* Goats rearing.
* Agriculture farming includes the growing of cash crops and vegetables e.g. Maize/corn growing, Beans growing, Coffee growing, Banana planting, Cassava growing, growing of Tomatoes , Cabbage growing, Carrots growing, Tree planting extra.

<br>
REGISTERED ACHIEVEMENTS AT OCIN-UGANDA

a. We have a humble office room located in Kadiba Zone.

b. We have a committed and faithful team which is sacrificially giving us funds on monthly basis namely; Brother Rahoul Bhagat, Jan

c. We have been able to give out scholastic materials for now 2 and half years.

d. We have established OCIN –Home for most desperate children and orphans 20+ in number.

e. We have been able to partly pay for the school fees of 20 orphans and children in need.

f. Two children are now being sponsored by a loving foster parent, their school fees is sent monthly.

g. We have also been able to give out clothes to the most needy.

h. Three times in a year we have been able to provide a hot meal for children exceeding 100 in number.

i. We have been able to acquire wooden decked beds for the children.

j. OCIN-Uganda is registered and recognized by the government of Uganda.

k. We have given out sandals 2 times since we began.

l. We have been providing scholastic materials for three consecutive times.

<br>
CHALLENGES MET AT OCIN-UGANDA.

a. Land where to construct permanent office building.

b. Lack of a permanent building to house desperate orphans and children in need and those who lives at risk.

c. Lack of office equipments to ease administration work that is to say photocopying machine, printers, computers,
vehicles or motor cycle for field work.

d. Lack of enough space where to establish the ministry self generating income projects, poultry, piggery, goat
rearing etc.

e. Lack of land where to practice agriculture farming to grow food, vegetables to feed the children.

f. Lack of enough funds to fulfill the mission, vision and objectives of OCIN-Uganda and employing permanent cook,
matron, social workers also meeting administration costs, meet medical bills for the sickly children.

g. Facing an overwhelming number of applicants seeking assistance.

h. Lack of startup capital fund to help us establish income generating activities.

i. Lack of enough of funds to pay rent in time some times.

j. Lack of enough funds to stock enough food for the children and orphans at the home.

k. Lack of mosquito nets, cooking souse pans, enough plates, and there is a shortage of beddings.

<br>
FUTURE PROSPECTS OF OCIN UGANDA

a. Establish income generating projects for the ministry.

b. Open up a ministry account in a recognized bank.

i. Ocin-Uganda current account.

ii. Ocin-Uganda Toto savings account.

iii. Ocin-Uganda term deposit account. (Quarterly, semi-annually, annually).

c. Write proposals to seek grants, donations, and in kind gifts etc.

d. Create more awareness locally and internationally to the likeminded people.

e. Give room for willing volunteers to come and display their skills at the ministry on short term basis.

f. Construct children’s home to minimize rent expenses.

g. To own an agricultural farm where we can grow our own food.

h. Start making arts and crafts-African jewelry (beads, necklaces and look for ready market locally and internationally.

i. Mobilize for individual sponsors for the children missing out on Education.

j. Develop a finance committee to help in financial planning and sustainability.

k. Form a children’s choir which can perform nationally and internationally for awareness raising and fundraising
concerts to boost existing donations.

l. To start up a school project for both nursery and primary school going children as to eradicate illiteracy in the community.

m. To start a vocational skills training center so as to empower children who have missed out on primary education
with a skill to live on in future.

n. To start up medical center so, as to boost the health condition of the people in the community where the project is
located.

o. To acquire a project van to ease transportation of children and their guardians for study tours, transportation to schools, picking ministry guest from the airport and also taking them around the project catchment areas.

p. Acquiring a project motor bike to ease transportation of the social worker while carrying home tracing and home visits, school visits, payment of school fees.

q. Acquiring of office laptops or computers to ensure proper documentation and record keeping.

r. Establishing a mission’s guest house as an income generating activity for Ocin-Uganda.

<br>
PARTNERSHIP OPPORTUNITIES AT OCIN-UGANDA

Prayer Partnership and Raising Awareness
We call for prayer partners and raising awareness about this ministry and divine call of reaching out to the suffering children in the community of Buwenge and beyond we covet for your effective involvement and representation.

Self Sustainability: OCIN has a goal to become self sustainable by 2030 with your advice, ideas and support we can be able to see this dream come to fruitfulness.

<br>
WHAT TO EXPECT FROM OCIN-UGANDA MANAGEMENT

Respect and Freedom.

a. All our esteemed friends; partners should have an assurance that their information about gifts will be handled with respect and confidentiality.

b. The relationship with our [partners representing the interest of the ministry will be professionally handled in nature.

c.  Our beloved partners will have the freedom to ask questions when making donation, it’s there right to receive a prompt and forth right answers it won’t be pushed too far or too hard.

<br>
GOOD SERVANT HOOD

We behave to receive all the gifts and donations gratefully safe guard them deliberately use them responsibly, lovingly in justice with others and return them with increased impact to the lord’s glory. We will live up to your expectations as men and woman of TRUST. We may not start big to gain your trust but let’s start small to gain your full confidence as we strengthen this meaningful working relationship.

<br>
THOUGHTS AND EXITING REMARKS

There is no doubt neither any question that; this is our divine calling and demanding mission. Through the management of Ocin_uganda we are happy that the ministry is growing into an effective force and a hand that is touching and impacting the lives of young and suffering children.

<br>
OUR INVITATION
We are happy to invite you  join us, in this rewarding mission as we touch thousands with a hand that amends and comforts, a voice that heals and restores the hope of the orphans and children in need
We all know that God is the father of the fatherless; you and OCIN-team becomes the nurturing mothers to deliver what we can; what I believe together we can bring a lasting solution and a smile to the crying child.

<br>
FOOD FORTHOUGHT

It’s very sad and painful to see and hear babies left alone on streets and hospital corridals. Every day around the world there is a child ailing for help and assistance. Sometimes it’s very difficult to understand the lives of others if you have never gone through such condition of life. How can someone give a life to a child if you don’t have a heart responding to the need in a call? It hurts so much to see some of these cases. Many of these children seen on the roadsides are going to be some bodies in the future; but without the help of kindhearted person these kids would die in hunger and desperation. Therefore these orphans and children in need on this planet earth need our help and support.

<br>

<center><a href="#top">Back to top</a></center>
