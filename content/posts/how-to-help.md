---
title: "How to Help"
date: 2018-01-28T20:31:16+01:00
draft: false
---

You can get involved in the following programs:

1) SPONSOR A CHILD<br>

Only $50 dollars a month enables OCIN Uganda to provide the child with the school tuitions, books, uniforms, a hot meal each day while at school. Medical attention. The child is assigned asocial worker who is responsible for making sure that child needs are met. The social work also helps the child write letters deliver packages from the sponsor and purchases any designated gift. For some one who feels to sponsors a child please write the director at his Email: nasitubrandon@gmail.com or to our ministry coordinators in the USA at their emails found on the reccomenders. Thank you.

You can send us money safely through <a href="http://www.wave.com" target="_blank">Wave</a> (free of charge) or <a href="http://www.worldremit.com" target="_blank">WorldRemit</a> to our registered mobile mobile money numbers:

* For MTN mobile money: <b>+256773945366</b>

* For Airtel mobile money: <b>+256702464172</b>

<br>

2) PRAYER PARTNER<br>
You can become our ministry prayer partner


<br><hr>

<font size=2>Volunteer editor's note: Cash transfers have arguably the strongest existing evidence base among anti-poverty tools. You can learn more about this form of intervention on <a href="https://www.givedirectly.org/research-on-cash-transfers">givedirectly.org/research-on-cash-transfers</a>.</size>
