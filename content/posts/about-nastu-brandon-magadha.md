---
title: "About Magadha Nastu Brandon"
date: 2018-01-28T20:32:31+01:00
draft: false
---

<center><img src=https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-9/26112271_1560456840708001_1598846609464793440_n.jpg?oh=8613be80710d7e08e28492050134f031&oe=5B1D41DD height=50% width=50%></img></center>

<b>Magadha Nastu Brandon</b><br/>
Executive Director and founder of the Orphans and Children in Need Uganda

<a mailto:nasitubrandon@gmail.com>nasitubrandon@gmail.com</a><br/>
+256772178790

P.O.BOX 4821<br/>
JINJA-UGANDA<br/>
EAST AFRICA

<a href="https://www.facebook.com/orphansandchildreninneeduganda" target="_blank">Join me on Facebook</a>

<hr><br>



Warm, Lovely, and Peaceful greetings to you. Hope you are fine with your health and shape. How are you, Family, and your parents? Convey my regards to everyone in your family and the community.

My name is Magadha Nastu Brandon, Born on the 2st-Feb-1990, in a family four (5) boys and one (1) girl. Partly grew up in busiya village, Jinja district, and Buwenge. My Siblings names are Isabirye Faisal, Said Jacob, and Oshen Nabirye Aisha. My father and the mother are a peasant whose names are Mwase and Ruth Kawala. It is sad that they both separated. 08st-August 2005 I received Christ as lord and savior got baptized after three months, my spiritual nurturing and growth started at Omega Christian prayer center in Buwenge, And Agape global faith church where I currently minister from. I live in Buwenge town council which is 25 kilometers from Jinja the second capital city of Uganda.

First of all I would like to sincerely thank you for all positive contribution done to your life, family, Country and the universe at large. I know you might be surprised at who is talking to you but honestly we have never met before but through this social network I have deliberately contacted you.

My purpose of writing to you is to take this chance of developing a friendship and a constructive working relationship an initiative that will be so amazing to learn many things like personal responsibility, leadership, success, fatherhood, create serving opportunities in different aspects, create  unique ways to cultivate abilities, gifts and talents, leverage resources, build man power and strengthening one another, discover ways of participating in our communities and around the world and extra. I will always treasure the time we shall spend, and know how to cherish the time I will be adopting new ideals until our stomach didn’t ask us to stop. The nature of work I do needs a growing working relationship that will benefit the people I serve and the community. I will be pleased to host you one time at your convenience in order to understand more about our ministry and church to enable us provide a better service. I do hope for your positive response towards my humble request.

‘’There is a saying two are better than one: because they have a good reward for their labor; for if they fall, the one will lift up his fellow; but woe to him that is alone when he falls for he has not another to help him up’’.

<div align="right">Magadha Nastu Brandon<br>
<font size=2>Executive Director and founder of
Orphans and Children in Need Uganda</font></div>


<hr><br>

MINISTRY BACKGROUND AND SERVICE

From 2007-2010 started serving the lord as a Sunday school teacher at the church, Choir member, choir leader, Youth chairman, Associate Pastor to a full Pastor.

My Service in ministry started at Agape global faith  church (where I served as Sunday school teacher and choir member), after some time I founded up Children care ministry  known as Orphans and children in need Uganda (OCIN) in 2011/01/20th

<br>

CURRENT RESPONSIBILITIES

1.	Am the Executive Director orphans and children in need Uganda and Founder, (where my active roles are to set goals, Innovate, make important decisions, set priorities, care, serve, communicate, correct, coach, cheer, captain, challenge, persuade, inspire, motivate, think, plan, link/liaise, always remain visionary).
2.	Chief Executive Director Real Estate soccer ministry

<br>

MARITAL STATUS

Am Single  

<br>

COMMUNITY OBSERVATION AND COMPLEMENT

All the years of service to the community I have been people describe me as an empathetic, compassionate,  ambitious, clear-headed, visionary, perceptive, patient, trustworthy, consistent, an excellent listener, a role model, Mentor, determined person, policy maker, Influential, friendly and social, a good communicator, a voice for the voiceless, a humanitarian, innovative, a good leader, source of inspiration and encouragement.

<br>

HOBBIES AND INTERESTS

In my life time I have discovered the Hobbies and Interests; Farming and gardening; making art from used materials and pottery; Reading, Travelling, Networking, Collecting stamps and letter writing, Play musical instruments, Birds and animal care, love doing Volunteer work and community service involvement, Community development, Cooking, Childcare and doing Humanitarian services, Sports and recreation, Club Memberships, surfing, internet browsing, blogging and social networking; exercise and body fitness, cycling, meditation; painting, creative writing, singing, and music composition, dance and drama, photography, doing window shopping and exploration; watching movies, Documentaries, News, soccer matches.

<br>

EDUCATION BACKGROUND

In my education journey started from Nursery, Primary, O’level and A ‘level, I joined institution where I obtained a certificate in children management and computer repairing and maintenance.

<br>

CULTIVATED KEY SKILLS

1.	Strong analysis and researching skill.
2.	Able to prioritize a broad range of responsibilities to meet a deadline, and a goal.
3.	 Fit to lead a team.
4.	Able to relate to others.
5.	 Good communication skill.
6.	 Friendly with a genuine interest in others.
7.	 Good and flexibility skill.
8.	 Ability to persuade others.
9.	Able to inspire, support and motivate others.

<hr><br>

On behalf of my family, the Church, and the entire Ministry receive our heartfelt love and greetings, and please extend our Love to your family members, friends and the community at large.<br>

Thank you; for your time; looking forward to read and hear from you!<br>

I remain yours faithfully in lord’s vineyard.

More information, comments please feel free to ask me directly I will happy to avail you with the required answers whenever possible.

<br>

<center><a href="#top">Back to top</a></center>
