---
title: "Recommendations"
date: 2018-01-28T20:33:20+01:00
draft: false
---

For recommendation letters, please write:

Mr Rahoul M. Bhagat<br>
<a mailto:rahoul.bhagat@evoqua.com>rahoul.bhagat@evoqua.com</a>


Mr. Jan Macdonald<br>
<a mailto:jan@bhgpartners.com>jan@bhgpartners.com</a>
