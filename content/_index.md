<center>
## <b>Orphans and Children in Need Uganda </b><br>OCIN-UGANDA

<font size=2>
P.O.BOX 4821<br>
JINJA-UGANDA<br>

+256 772 178 790 &emsp;
+256 757 327 555<br>

ocinuganda@gmail.com

</font></center><br>

[Orphans and Children in Need Uganda](../hugo/posts/about-ocin) is a total child care ministry committed to rescue and meet the basic needs of the suffering children and orphans who have lost their parents due to HIV/AIDS mainly though there might be other factors these include family splits, poverty, civil wars and internal displacement.

The ministry was founded and is led by [Mr. Brandon Nasitu Magadha](../hugo/posts/about-nastu-brandon-magadha) whom God touched to help the suffering, desperate and abandoned children who were roaming on the streets of Buwenge as Hawkers. It reached a time where he could not help it out until he acted with the help and guidance of his pastor then Orphans and children in need began to receive [partial support](../hugo/posts/how-to-help).
